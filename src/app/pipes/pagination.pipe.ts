import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'paginate'})
export class PaginationPipe implements PipeTransform {
    transform(value: any[], totalItems: any): any {
      console.log(value, totalItems, ...value.slice( 3*(totalItems-1) , 3*(totalItems) ));
        return [ ...value.slice( 3*(totalItems) , 3*(totalItems+1)  )]
        // return [...value]
    }
}