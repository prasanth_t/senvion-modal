import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './_components';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MenuListItemComponent } from './menu-list-item/menu-list-item.component';
import { MaterialModule } from './material/material.module';

import { AgGridModule } from 'ag-grid-angular';

import { MatIconModule } from '@angular/material/icon'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';


import {NgbModule, NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DropdownCommonService } from './_services/dropdown-common.service';
import { DashboardService } from './_services/dashboard.service';
import { MomDashboardComponent } from './mom-dashboard/mom-dashboard.component';
import { CreateMeetingDetailComponent } from './create-meeting-detail/create-meeting-detail.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PaginationPipe } from './pipes/pagination.pipe'

//import { SidebarService } from './_services/sidebar.service';
//import { NavService } from './_services/nav.service';

import { NgxSmartModalModule,NgxSmartModalService } from 'ngx-smart-modal';
import { DashboardModule } from './dashboard/dashboard.module';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    SidebarComponent,
    MenuListItemComponent,
    MomDashboardComponent,
    CreateMeetingDetailComponent,
    PaginationPipe
  ],
  imports: [
    DashboardModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AgGridModule,
    MatIconModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,    
    NgbModule,
    NgbPaginationModule, 
    NgbAlertModule,
    NgxPaginationModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [
    NgxSmartModalService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    DropdownCommonService,
    DashboardService,
    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
