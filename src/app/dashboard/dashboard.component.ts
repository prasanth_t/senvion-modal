import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, ViewEncapsulation, forwardRef  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
//import { ColDef, GridReadyEvent, CheckboxSelectionCallbackParams,  GridApi, HeaderCheckboxSelectionCallbackParams, CellClickedEvent, ICellEditorParams, RowNodeTransaction,   CellEditRequestEvent, GetRowIdFunc, GetRowIdParams, CellValueChangedEvent,  ICellEditorComp,  RowValueChangedEvent } from 'ag-grid-community';
import { Observable, of } from 'rxjs';
import { DropdownCommonService } from '../_services/dropdown-common.service';
import { DashboardService } from '../_services/dashboard.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, NgForm, FormArray, FormControl, FormGroup, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { User } from '../User';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DashboardComponent),
      multi: true
    }
  ]
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  [x: string]: any;

  dashboardsearchform!: FormGroup;

  protected users!: any[];

  protected countries!: any[];

  public userMultiCtrl: FormControl = new FormControl();

  public dateCtrl: FormControl = new FormControl();

  public userMultiFilterCtrl: FormControl = new FormControl();

  public filteredUsersMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  @ViewChild('multiSelect') multiSelect!: MatSelect;

  protected _onDestroy = new Subject<void>();

  @ViewChild('myform') form! : NgForm;
  //country = [];
  public country_dropdown_records: any ;
  public project_dropdown_records: any ;
  public document_dropdown_records: any ;
  public meeting_dropdown_records: any ;
  records:any;
  p: number = 1;
  total: number = 0;

  constructor(private http: HttpClient, private formBuilder: FormBuilder, public DropdownCommonService: DropdownCommonService , private dashboardService: DashboardService,
    public ngxSmartModalService: NgxSmartModalService, public router: Router) { 
      
    }

  ngOnInit(): void {

    this.dashboardsearchform = this.formBuilder.group({
      project_type:[''],
      document_type: [''],
      country: [''],
      meeting_type: ['']
      
    });

    this.dashboardService.getUsers().subscribe((response: any) => {
      this.users = response;
      this.filteredUsersMulti.next(this.users.slice());
    });

    this.dashboardService.getCountries().subscribe((response: any) => {
      this.countries = response;
    });

    this.userMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUsersMulti();
      });

    /**
     * Country Dropdown
     * 
     */
    this.DropdownCommonService.getAllCountry().subscribe(countrydata => {
    this.country_dropdown_records = countrydata;
     // this.dashboardsearchform.controls['country'].patchValue(this.records[0].id);
    });

    /**
     * Project type Dropdown
     */
    this.DropdownCommonService.getAllProjectType().subscribe(projecttypedata => {
      this.project_dropdown_records = projecttypedata;
      //console.log(this.project_dropdown_records);
       // this.dashboardsearchform.controls['project_type'].patchValue(this.records[0].id);
    });

    /**
     * Document type Dropdown
     */
    this.DropdownCommonService.getAllDocumentType().subscribe(documenttypedata => {
      this.document_dropdown_records = documenttypedata;
      //console.log(this.document_dropdown_records);
       // this.dashboardsearchform.controls['project_type'].patchValue(this.records[0].id);
    });

    /**
     * Meeting type Dropdown
     */
    this.DropdownCommonService.getAllMeetingType().subscribe(mettingtypedata => {
      this.meeting_dropdown_records = mettingtypedata;
      //console.log(this.meeting_dropdown_records);
       // this.dashboardsearchform.controls['project_type'].patchValue(this.records[0].id);
    });

    this.getDashboardRecords();
  }

  ngAfterViewInit(){
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredUsersMulti
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.multiSelect.compareWith = (a: User, b: User) => a && b && a.id === b.id;
      });
  }

  getRequestParams(project_type: string, country: string, document_type: string, meeting_type:string): any {
    let params: any = {};

    if (project_type) {
      params[`project_type`] = project_type;
    }
    if (country) {
      params[`country`] = country;
    }
    if (project_type) {
      params[`document_type`] = document_type;
    }
    if (meeting_type) {
      params[`meeting_type`] = meeting_type;
    }
    return params;
  }

   /**
   * getDashboardRecords
   * get the record
   * @return response()
   */
   getDashboardRecords(){
   // const params = this.getRequestParams(this.project_type, this.country, this.document_type, this.meeting_type);
      this.dashboardService.getAll(this.p).subscribe((response: any) => {
        this.records = response.data;
        this.total = response.total;
      });
    } 

  /**
   * Write code on Method
   *
   * @return response()
   */
  pageChangeEvent(event: number){
      this.p = event;
      this.getDashboardRecords();
  }

  onSubmit(){
    alert(this.dashboardsearchform.value)
    console.log(this.dashboardsearchform.value);
  }

  onEdit(entry: any){
    this.ngxSmartModalService.open('myModal');
    this.userMultiCtrl.setValue([this.users[1], this.users[5], this.users[9]]);
    this.dateCtrl.setValue(new Date(2023, 1, 12));
    this.form.setValue({
      meetingId:entry.id,
      modelcode:entry.meetingsubject,
      country:'DE',
      // mdate:'1/12/2023',
      meetingtype:'2',
      // users:'',
    });
  }

  onClose(){
    this.form.reset();
    this.userMultiCtrl.reset();
    this.dateCtrl.reset();
    this.ngxSmartModalService.close('myModal');
  }

  meetingForm(form: NgForm){
    var val = form.controls;
    var updatedData = {'meetingId': val['meetingId'].value, 'modelcode': val['modelcode'].value,
    'country': val['country'].value, 'mdate': this.dateCtrl.value,
    'meetingtype': val['meetingtype'].value, 'users': this.userMultiCtrl.value };
    this.dashboardService.addUpdateMeeting(updatedData).subscribe(res => {
      this.router.navigate(['/activity/10']);
    }, err => {
  
    });
  }

  onNew(){
    this.ngxSmartModalService.getModal('myModal').open();
  }

  protected filterUsersMulti() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.userMultiFilterCtrl.value;
    if (!search) {
      this.filteredUsersMulti.next(this.users.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredUsersMulti.next(
      this.users.filter(users => users.name.toLowerCase().indexOf(search) > -1)
    );
  }

}
