import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validator} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
//import { ColDef, GridReadyEvent, CheckboxSelectionCallbackParams,  GridApi, HeaderCheckboxSelectionCallbackParams, CellClickedEvent, ICellEditorParams, RowNodeTransaction,   CellEditRequestEvent, GetRowIdFunc, GetRowIdParams, CellValueChangedEvent,  ICellEditorComp,  RowValueChangedEvent } from 'ag-grid-community';
import { DropdownCommonService } from '../_services/dropdown-common.service';

@Component({
  selector: 'app-mom-dashboard',
  templateUrl: './mom-dashboard.component.html',
  styleUrls: ['./mom-dashboard.component.css']
})
export class MomDashboardComponent implements OnInit {

  dashboardsearchform!: FormGroup;
  //country = [];
  public records: any ;

  constructor(private http: HttpClient, private formBuilder: FormBuilder, public DropdownCommonService: DropdownCommonService) { }

  ngOnInit(): void {

    this.dashboardsearchform = this.formBuilder.group({
      project_type:[''],
      document_type: [''],
      country: [''],
      meeting_type: ['']
      
    });

     // async orders
     this.DropdownCommonService.getAllCountry().subscribe(countrydata => {
      this.records = countrydata;
      console.log('country', this.records);
     // this.dashboardsearchform.controls['country'].patchValue(this.records[0].id);
    });
  }

  onSubmit(){
    alert(this.dashboardsearchform.value)
    console.log(this.dashboardsearchform.value);
  }

}
