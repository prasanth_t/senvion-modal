import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
//import { NavService } from '../_services/nav.service';
//import { SidebarService } from '../_services/sidebar.service';
import { NgForm, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {

  sidenavWidth = 6;

  //ngStyle: string;
  constructor(public ngxSmartModalService: NgxSmartModalService) {

  }

  ngOnInit() {}

  increase() {
    this.sidenavWidth = 15;
    // console.log('increase sidenav width');
  }
  decrease() {
    this.sidenavWidth = 4;
    // console.log('decrease sidenav width');
  }

  // sidenavToggle() {
  //   this.ngStyle = 'this.sidenavWidth = 15';
  //   console.log('sidenav width incrases');
  // }

  onNew(){
    this.ngxSmartModalService.getModal('myModal').open();
  }

}
