import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CreateMeetingDetailComponent } from './create-meeting-detail/create-meeting-detail.component';
import { MomDashboardComponent } from './mom-dashboard/mom-dashboard.component';

import { AuthGuard } from './_helpers';

const dashboardModule = () => import('./dashboard/dashboard.module').then(x => x.DashboardModule);




const routes: Routes = [
  { path: '', component: LoginComponent },
  // { path: 'dashboard', loadChildren: dashboardModule, canActivate: [AuthGuard] },
  { path: 'dashboard', loadChildren: dashboardModule},
  { path: 'create-meeting-detail', component: CreateMeetingDetailComponent, canActivate: [AuthGuard]},
  { path: 'mom-dashboard', component: MomDashboardComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
