import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DropdownCommonService {

  private apiURL = 'http://localhost:4200/assets/';

  constructor( private http: HttpClient ) { }

  /**
   * getAllCountry
   * Get all country
   * @returns Json
   */

  getAllCountry(): Observable<any[]> {
    //alert(this.apiURL+'country.json');
    return this.http.get<any[]>(this.apiURL+'country.json')
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * getAllProjectType
   * Get all project type
   * @returns Json
   */
  getAllProjectType(): Observable<any[]> {
    //alert(this.apiURL+'country.json');
    return this.http.get<any[]>(this.apiURL+'project_type.json')
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * getAllDocumentType
   * Get all Document type
   * @returns Json
   */
  getAllDocumentType(): Observable<any[]> {
      //alert(this.apiURL+'country.json');
      return this.http.get<any[]>(this.apiURL+'document_type.json')
      .pipe(
        catchError(this.errorHandler)
      );
  }

  /**
   * getAllMeetingType
   * Get all Document type
   * @returns Json
  */
  getAllMeetingType(): Observable<any[]> {
      //alert(this.apiURL+'country.json');
      return this.http.get<any[]>(this.apiURL+'meeting_type.json')
      .pipe(
        catchError(this.errorHandler)
      );
  }

  



  /**
   * Errors handler
   * function for error handling
   * @param error 
   * @returns  
   */
 errorHandler(error: { error: { message: string; }; status: any; message: any; }) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(() => errorMessage);
}


}
