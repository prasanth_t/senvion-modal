import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private sidenavOpenSubject : Subject<boolean>;

  constructor() {
      this.sidenavOpenSubject = new Subject<boolean>();
  }

  toggleSideNav(isStatusTrue: boolean): void {
      this.sidenavOpenSubject.next(isStatusTrue);
  }

  onSideNavToggle(): Observable<boolean> {
      return this.sidenavOpenSubject;
  }

  isExpandable(val:boolean){
   return val;
  }
}
