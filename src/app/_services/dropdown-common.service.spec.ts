import { TestBed } from '@angular/core/testing';

import { DropdownCommonService } from './dropdown-common.service';

describe('DropdownCommonService', () => {
  let service: DropdownCommonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DropdownCommonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
