import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private apiURL = 'http://localhost:4200/assets/';


  constructor( private http: HttpClient ) { }

  getAll(page: number){
    //alert(this.apiURL+'dashboard.json' + '?page=' + page)
    return this.http.get(this.apiURL+'dashboard.json' + '?page=' + page);
  }

  getUsers(){
    //alert(this.apiURL+'dashboard.json' + '?page=' + page)
    return this.http.get(this.apiURL+'users.json');
  }

  getCountries(){
    //alert(this.apiURL+'dashboard.json' + '?page=' + page)
    return this.http.get(this.apiURL+'countries.json');
  }

  addUpdateMeeting(data: any){
    let jsonData = JSON.stringify(data);
    return this.http.post(this.apiURL, jsonData);
  }
}
