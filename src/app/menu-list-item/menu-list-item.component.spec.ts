import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListItemComponent } from './menu-list-item.component';
import { NavService } from '../services/nav.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from "@angular/router/testing";
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { SidebarService } from '../services/sidebar-service';
import { RouterModule, Routes } from '@angular/router';

describe('MenuListItemComponent', () => {
  let component: MenuListItemComponent;
  let fixture: ComponentFixture<MenuListItemComponent>;
  let service: NavService;
  let sidebarService:SidebarService;
  const routes: Routes = [{ 
      path: 'dashboard',
      loadChildren: () => import('src/app/dashboard/dashboard.module').then(m => m.DashboardModule) 
     }
  ];
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuListItemComponent ],
      imports:[HttpClientTestingModule, HttpClientModule, TranslateModule.forRoot({
        defaultLanguage: 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }), 
      RouterTestingModule.withRoutes(routes)],
      providers:[NavService,SidebarService,RouterTestingModule],
    })
    .compileComponents();

    fixture = TestBed.createComponent(MenuListItemComponent);
    service = TestBed.inject(NavService);
    sidebarService = TestBed.inject(SidebarService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
