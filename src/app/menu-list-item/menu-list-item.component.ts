import { Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import { NavItem } from '../_services/nav-item';
import { Router } from '@angular/router';
import { NavService } from '../_services/nav.service';
import { state, style, trigger} from '@angular/animations';
import { SidebarService } from '../_services/sidebar.service';

@Component({
  selector: 'menu-list-item',
  templateUrl: './menu-list-item.component.html',
  styleUrls: ['./menu-list-item.component.css'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
    ])
  ]
})
export class MenuListItemComponent implements OnInit {
  expanded: boolean = false;
  @Input() item!: NavItem;
  @Input() depth: number = 0;
  isExpandableTrue :boolean = true;   
  showOnHover : boolean = false;
  @ViewChildren('menuItemList') menuItemList!: QueryList<any>;
  constructor(public navService: NavService,public sidebarService:SidebarService, public router: Router) {
    this.sidebarService.onSideNavToggle().subscribe(
          (isStatusTrue) => {
            this.isExpandableTrue = isStatusTrue;
          }
      );
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {
    // Do nothing
  }

  onItemSelected(item: NavItem) {
    if(!this.expanded && item != undefined && item.children != undefined && item.children.length > 0){
      this.menuItemList.forEach((e) => {
           e.style.display = 'none';
      });
    }
    if (!item.children || !item.children.length) {
      this.router.navigate([item.route]);
    }

    if (item.children && item.children.length) {
      this.expanded = !this.expanded;
    }
    
  }

  mouseEnter(){
    this.expanded = !this.expanded;
  }

  mouseLeave(){
    this.expanded = !this.expanded;
  }

}
