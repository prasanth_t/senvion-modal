import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMeetingDetailComponent } from './create-meeting-detail.component';

describe('CreateMeetingDetailComponent', () => {
  let component: CreateMeetingDetailComponent;
  let fixture: ComponentFixture<CreateMeetingDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMeetingDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateMeetingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
