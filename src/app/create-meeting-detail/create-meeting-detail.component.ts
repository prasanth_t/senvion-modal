import { Component, OnInit, ViewChild } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbDatepicker} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-create-meeting-detail',
  templateUrl: './create-meeting-detail.component.html',
  styleUrls: ['./create-meeting-detail.component.css']
})
export class CreateMeetingDetailComponent implements OnInit {

  closeResult = '';

  constructor( private modalService: NgbModal ) { }

  @ViewChild('d', { read: NgbDatepicker }) d!: NgbDatepicker;

  ngOnInit(): void {
  }

  open(content:any) {
    this.modalService.open(content,
   {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = 
         `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
